namespace RomanNumerals.Cli
{
    public struct ConversionTuple
    {
        public int Arabic;
        public string Roman;

        public ConversionTuple(int arabic, string roman)
        {
            Arabic = arabic;
            Roman = roman;
        }
    }
}
