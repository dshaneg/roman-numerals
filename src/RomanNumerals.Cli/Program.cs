﻿using System.Collections.Generic;
using RomanNumerals.Domain;

namespace RomanNumerals.Cli
{
    class Program
    {
        static void Main()
        {
            var conversions = GetConversions();
            var reporter = new Reporter(conversions);

            reporter.Report();
        }

        private static IEnumerable<ConversionTuple> GetConversions()
        {
            var conversions = new List<ConversionTuple>();

            for(int arabic = RomanNumeralConverter.MIN_ARABIC; arabic <= RomanNumeralConverter.MAX_ARABIC; arabic++)
                conversions.Add(new ConversionTuple(arabic, RomanNumeralConverter.ToRoman(arabic)));

            return conversions;
        }
    }
}
