using System;
using System.Collections.Generic;
using System.Linq;

namespace RomanNumerals.Cli
{
    public class Reporter
    {
        private readonly IEnumerable<ConversionTuple> _conversions;

        public Reporter(IEnumerable<ConversionTuple> conversions)
        {
            _conversions = conversions ?? throw new ArgumentNullException(nameof(conversions));
        }

        public void Report()
        {
            WriteHeader();
            WriteBody();
        }

        private static void WriteHeader()
        {
            Console.WriteLine($"# Roman Numerals\n");
            Console.WriteLine("| Arabic | Roman |");
            Console.WriteLine("| --- | --- |");
        }

        private void WriteBody()
        {
            foreach(var conversion in _conversions)
                Console.WriteLine($"| {conversion.Arabic} | {conversion.Roman} |");
        }
    }
}
