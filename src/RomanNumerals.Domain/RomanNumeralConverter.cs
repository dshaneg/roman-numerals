﻿using System;
using System.Linq;
using System.Text;

namespace RomanNumerals.Domain
{
    public static class RomanNumeralConverter
    {
        public static readonly int MIN_ARABIC = 1;
        public static readonly int MAX_ARABIC = 3999;

        // The order of the entries is important to the algorithm
        private static readonly MapEntry[] valueMaps = new MapEntry[]
        {
            new MapEntry(1000, "M"),
            new MapEntry(900, "CM"),
            new MapEntry(500, "D"),
            new MapEntry(400, "CD"),
            new MapEntry(100, "C"),
            new MapEntry(90, "XC"),
            new MapEntry(50, "L"),
            new MapEntry(40, "XL"),
            new MapEntry(10, "X"),
            new MapEntry(9, "IX"),
            new MapEntry(5, "V"),
            new MapEntry(4, "IV"),
            new MapEntry(1, "I"),
        };

        public static string ToRoman(int arabic)
        {
            if (arabic < MIN_ARABIC)
                throw new ArgumentOutOfRangeException(nameof(arabic), "Romans don't understand zero or negative numbers.");
            if (arabic > MAX_ARABIC)
                throw new ArgumentOutOfRangeException(nameof(arabic), "Converter doesn't handle numbers above 3999.");

            return BuildRoman(arabic);
        }

        private static string BuildRoman(int arabic)
        {
            int remaining = arabic;
            var sb = new StringBuilder();

            while (remaining > 0)
            {
                MapEntry entry = FindGreatestMapEntryUpToValue(remaining);
                remaining -= entry.Arabic;
                sb.Append(entry.Roman);
            }

            return sb.ToString();
        }

        private static MapEntry FindGreatestMapEntryUpToValue(int value)
        {
            return valueMaps.First(entry => entry.Arabic <= value);
        }

        private struct MapEntry
        {
            public int Arabic;
            public string Roman;

            public MapEntry(int arabic, string roman)
            {
                Arabic = arabic;
                Roman = roman;
            }
        }
    }
}
