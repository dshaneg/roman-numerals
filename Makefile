test:
	dotnet test tests/UnitTests/UnitTests.csproj --no-restore --nologo

# comparing the time it takes this command to run vs the test rule's command shows that you can save a lot of time by being specific
# time make test
# vs
# time make test-raw
test-raw:
	dotnet test

run:
	dotnet run -p src/RomanNumerals.Cli > roman.md
