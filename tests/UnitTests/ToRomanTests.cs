using System;
using RomanNumerals.Domain;
using Xunit;

namespace UnitTests
{
    public class ToRomanTests
    {
        [Theory]
        [InlineData(int.MinValue)]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(4000)]
        [InlineData(int.MaxValue)]
        public void ToRoman_GivenInputOutOfRange_ThrowsException(int arabic)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => RomanNumeralConverter.ToRoman(arabic));
        }

        [Theory]
        [InlineData(1, "I")] // Acceptance Test
        [InlineData(2, "II")]
        [InlineData(3, "III")] // Acceptance Test
        [InlineData(4, "IV")]
        [InlineData(5, "V")]
        [InlineData(9, "IX")] // Acceptance Test
        [InlineData(10, "X")]
        [InlineData(11, "XI")]
        [InlineData(12, "XII")]
        [InlineData(13, "XIII")]
        [InlineData(20, "XX")]
        [InlineData(25, "XXV")]
        [InlineData(40, "XL")]
        [InlineData(42, "XLII")]
        [InlineData(44, "XLIV")]
        [InlineData(48, "XLVIII")]
        [InlineData(49, "XLIX")]
        [InlineData(50, "L")]
        [InlineData(80, "LXXX")]
        [InlineData(90, "XC")]
        [InlineData(99, "XCIX")]
        [InlineData(100, "C")]
        [InlineData(400, "CD")]
        [InlineData(500, "D")]
        [InlineData(900, "CM")]
        [InlineData(1000, "M")]
        [InlineData(3000, "MMM")]
        [InlineData(1066, "MLXVI")] // Acceptance Test
        [InlineData(1989, "MCMLXXXIX")] // Acceptance Test
        [InlineData(3999, "MMMCMXCIX")]
        public void ToRoman_GivenValidArabic_ReturnsRoman(int arabic, string expectedRoman)
        {
            var roman = RomanNumeralConverter.ToRoman(arabic);

            Assert.Equal(expectedRoman, roman);
        }
    }
}
